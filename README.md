# ZZ1 - Analyse de Données

## Organisation des séances :

### Cours :
+ Séance 1 (07/02) -> Statistique Descriptive (1) :
    + Définitions
    + Pré-traitement des données 
    + Statistique descriptive univariée

+ Séance 2 (14/02) -> Statistique Descriptive (2) :
    + Statistique descriptive bivariée

    
### TD / TP :
+ TP0 + TP1 (14/02) : -> Statistique Descriptive
    + Statistique Univariée
    + Statistique Bivariée



## Ensemble du cours 

[Cours de M.Barra](https://vbarra.github.io/ADbook/intro.html)