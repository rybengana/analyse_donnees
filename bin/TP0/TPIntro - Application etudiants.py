#!/usr/bin/env python
# coding: utf-8

# # Application - régression linéaire
# Pour prendre en main l'ensemble des outils, on propose de réaliser une régression linéaire par deux méthodes : 
# - en utilisant directement scikit-learn
# - en résolvant le système aux équations normales (voir cours analyse numérique)

# In[6]:


import numpy as np
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')

get_ipython().system('python3 -m  pip install --user sympy')

from sympy import init_printing, Matrix, symbols
from IPython.display import Image
from warnings import filterwarnings

init_printing(use_latex = 'mathjax')
filterwarnings('ignore')


# On se donne un ensemble de points du plan, on souhaite trouver la droite qui passe au mieux par ces points.
# Soit par exemple les points $(1,1)^T, (2,2)^T, (3,2)^T$

# In[23]:


t = np.array([1, 2, 3]).reshape((-1, 1))
y = np.array([1,2,2])


# ## Approche moindres carrés - rappels

# Une équation de droite est de la forme
# $$ y=a+b{t} $$
# et on demande à ce que les points passent par cette droite
# $$\begin{array}{ccl} 
# a+b&=&1 \\ a+2b &=& 2 \\ a+3b&=&2 \end{array}$$
# 
# Si c'est le cas, le système a une solution. Sinon, cela veut dire que $\begin{bmatrix} 1\\2\\2\end{bmatrix}\notin Im(A)$ avec $A=\begin{bmatrix} 1&1\\1&2\\1&3\end{bmatrix}$
# 
# $$ \begin{bmatrix} 1 & 1 \\ 1 & 2 \\ 1 & 3 \end{bmatrix}\begin{bmatrix} a \\ b \end{bmatrix}=\begin{bmatrix} 1 \\ 2 \\ 2 \end{bmatrix} $$
# On résout alors 
# $$ { A }^{ T }A { x } ={ A }^{ T } y$$
# avec pour solution
# $$ \hat { x } ={ \left( { A }^{ T }A \right)  }^{ -1 }{ A }^{ T }y $$
# 
# En pratique on utilise aussi la décomposition $QR$ de $A$ et on résout $Rx=Q^Ty$

# Ecrire la résolution de ce problème en `numpy` par le système aux équations normales et par la décomposition QR. Pour cette dernière, on pourra utiliser la fonction [qr](https://numpy.org/doc/stable/reference/generated/numpy.linalg.qr.html).

# In[28]:


#TODO


# ## Utilisation de sklearn
# À l'aide de la fonction [LinearRegression](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html), résolvez le problème de régression linéaire.

# In[12]:


#TODO

print("droite : y = %f + %f t" %(droite.intercept_,droite.coef_))



# ## Utilisation de la définition statistique
# Utiliser la formule vue en cours (en fonction de la variance et de la covariance) pour calculer la droite de régression
# si $y = ax + b$ alors 
# $$a = \frac{\sigma_{xy}}{\sigma_x^2}\quad\textrm{et}\quad b = \bar{y}-a\bar{x}$$

# In[ ]:


#TODO


# ## Affichage
# Afficher avec matplotlib le résultat de votre régression

# In[18]:


#TODO

