#!/usr/bin/env python
# coding: utf-8

# # Quelques éléments de Python 
# Python est un langage de programmation multiparadigme de haut niveau, typographié dynamiquement, assez proche d'un pseudo-code.
# 

# ## Types de base

# ### Nombres

# In[11]:


x = 5
print(str(x)+" est de type "+str(type(x)))


# In[73]:


x = 5.
y = 4
print(str(x)+" est de type "+str(type(x)))

print('\n Opérations')
print('Addition ', x + y - 1)   # Addition
print('Multiplication ',x * 2)   # Multiplication
print('Puissance', x ** y)  # Puissance


# ### Booléens

# In[78]:


t, f = True, False

print("t :",t," f : ",f, "de type :", str(type(t))) 
print("ET : ",t and f) 
print("OU : ",t or f)  
print("NOT : ", not t)   
print("XOR : ",t != f)  


# ### Chaînes de caractères (voir [documentation](https://docs.python.org/3/library/stdtypes.html#string-methods))

# In[27]:


zz1 = 'ZZ1'   
isima = "ISIMA"   
print(zz1+isima + " de longueur", len(zz1)+len(isima))
print ("zz%d %s"%(1,isima))


# In[31]:


s = "isima"
print(s.capitalize())  
print(s.upper())       
print(s.center(7))     
print(s.replace('i', 'Z')) 


# ### Conteneurs : listes, dictionnaires, sets, tuples

# #### ----------------------------------------------------
# #### Listes : équivalent d'un tableau, mais de taille variable et contenant des éléments de différents types ([documentation](https://docs.python.org/3/tutorial/datastructures.html#more-on-lists))
# #### ----------------------------------------------------
# 

# In[35]:


print('\n *** Manipulations de base ***') 
l = [3, -4, -2, 4, 5]   
print("liste :",l, "de troisième terme", l[2])
print("depuis la fin de la liste : ",l[-2])   
l[2] = 'CHANGE'  
print("liste :",l, "de troisième terme : ", l[2])
l.append('AJOUT')
print("liste :",l, "de dernier terme : ", l[-1])
v = l.pop()
print("liste :",l, "de dernier terme : ", l[-1], "j'ai dépilé : ",v)
print('\n *** Accès aux éléments de la liste ***') 
for elem in l:
    print(elem,' : élement de la liste')

print('\n *** Autre accès aux éléments de la liste ***') 
for i, elem in enumerate(l):
    print('élément %d: %s' % (i + 1, elem))

print('\n *** Opérations sur les sous-listes (slicing) ***')
l = list(range(5))    
print("liste : ",l)         
print("sous-liste [2:3] : ",l[2:4])    
print("sous-liste [2:fin] : ",l[2:])     
print("sous-liste [début:1] : ",l[:2])     
print("liste : ",l[:])     
print("liste sans le dernier élément : ",l[:-1])    
l[2:4] = ['A', 'B'] 
print("changement de valeurs par une sous-liste :", l)  


# Application d'une fonction (avec ou sans condition) à une liste

# In[33]:


l = list(range(5))    
carre = [x**2 for x in l if x % 2 == 0]
print(carre)


# #### ----------------------------------------------------
# #### Dictionnaires : couples (clé, valeur) ([documentation](https://docs.python.org/3/library/stdtypes.html#dict))
# #### ----------------------------------------------------
# 

# In[49]:


d = {'pantalon': 'rouge', 't-shirt': 'blanc','pull':'vert'}  
print('couleur du pantalon : ',d['pantalon'])      
print('Y a t il un pantalon dans le dictionnaire ?','pantalon' in d)     
print('dans le dictionnaire, la couleur du pull est ',d.get('pull', 'N/A')) 

print('\n *** Accès aux éléments du dictionnaire ***')
for p in d:
    couleur = d[p]
    print('le %s est %s'%(p,couleur))

print('\n *** Autre accès aux éléments du dictionnaire ***')
for p,c in d.items():
    print('le %s est %s'%(p,c))


# #### ----------------------------------------------------
# #### Sets : collection non ordonnée d'éléments distincts
# #### ----------------------------------------------------
# 

# In[63]:


vetements = {'pull', 't-shirt'}
vetements.add('pantalon')
print("Nombre d'éléments : ",len(vetements))      
vetements.add('pull')
print("Nombre d'éléments (élément ajouté déjà présent) : ",len(vetements))      

print('\n *** Accès aux éléments  ***')
for i, v in enumerate(vetements):
    print('élément %d: %s' % (i + 1, v))

print('\n Transformation')
print({s.upper() for s in vetements})


# #### ----------------------------------------------------
# #### Tuples : liste ordonnée de valeurs, très similaire à une liste. La pincipale différence est qu'elle peut être utilisée comme clé dans un dictoinnaire et comme élément d'un set.
# #### ----------------------------------------------------
# 

# In[70]:


d = {(x, x **2): x for x in range(10)}  
t = (5, 25)       
print('dictionnaire : ',d)
print('valeur de la clé t : ', d[t])       


# ## Fonctions
# définies avec le mot clé `def`, avec des arguments obligatoires, et d'autres optionnels

# In[87]:


def mafonction(x,maj=False):
    if x >= 0:
        return x**2-1
    else:
        if(not maj) :
             return'Je ne calcule pas pour ',x 
        else:
             return 'JE NE CALCULE PAS pour ',x  


for x in [-2, 0, 2]:
    print(mafonction(x))
for x in [-2, 0, 2]:
    print(mafonction(x,maj=True))

